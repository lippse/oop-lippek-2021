
public class Stack implements IntStack {

	private int firstFreeIndex = 0;
	private int[] memory;
	private int enlargeBy = 1;

	public Stack(int length) {
		this.memory = new int[length];
	}

	@Override
	public void push(int newNumber) {
		if (this.firstFreeIndex >= this.memory.length) {
			int[] temp = new int[this.memory.length + enlargeBy];
			for (int i = 0; i < this.memory.length; i++)
				temp[i] = this.memory[i];
			this.memory = temp;
		}
		memory[this.firstFreeIndex++] = newNumber;
	}
	
	@Override
	public int pop() {
		firstFreeIndex--;
		if (this.firstFreeIndex < 0) {
			System.out.println("Stack empty!");
			firstFreeIndex++;
			return -1;
		}
		int pulledNumber = memory[firstFreeIndex];
		return pulledNumber;
	}

	public int getEnlargeBy() {
		return enlargeBy;
	}

	public void setEnlargeBy(int enlargeBy) {
		this.enlargeBy = enlargeBy;
	}
}
