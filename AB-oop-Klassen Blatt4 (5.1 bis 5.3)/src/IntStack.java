
public interface IntStack {
	
	public void push(int x);
	
	public int pop();
	
}
