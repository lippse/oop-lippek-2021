public class StackCharlotte implements IntStack {

        private int[] zahlenliste;
        private int cpos = 0;

        public StackCharlotte(int anzahlElemente) {
                this.zahlenliste = new int[anzahlElemente];
        }

        @Override
        public void push(int i) {
                zahlenliste[cpos] = i;
                cpos++;
        }

        @Override
        public int pop() {
                if (cpos > 0) {
                        cpos--;
                        return zahlenliste[cpos];
                }
                return cpos;
        }

        public void stackausgabe() {
                System.out.print("[");
                for (int j = 0; j < this.cpos; j++) {
                        if (j == (this.cpos - 1))
                                System.out.print(zahlenliste[j]);
                        else
                                System.out.print(zahlenliste[j] + ",");
                }
                System.out.println("]");
        }

        @Override
        public String toString() {
                String ergebnis = "[";
                for (int j = 0; j < this.cpos; j++) {
                        if (j == (this.cpos - 1))
                                ergebnis = ergebnis + zahlenliste[j];
                        else
                                ergebnis = ergebnis + zahlenliste[j] + ",";
                }
                ergebnis = ergebnis + "]";

                return ergebnis;
        }
}