
public class TestStack {

	public static void main(String[] args) {

		IntStack s1 = new Stack(2);

		s1.push(5);

		System.out.println(s1.pop());

		s1.push(2);
		s1.push(3);
		s1.push(7);
		s1.push(9);

		for (int i = 0; i < 5; i++)
			System.out.println(s1.pop());
	}
}