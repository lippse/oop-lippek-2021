
public class DatumTest {

	public static void main(String[] args) {
		Datum d1, d2, d3;
		d1 = new Datum(1, 1, 1999);
		d2 = new Datum(1, 1, 2000);
		d3 = new Datum(1, 3, 1999);
		
		int x = d1.compareTo(d2);
		System.out.println(x);
		x = d2.compareTo(d3);
		System.out.println(x);
		x = d1.compareTo(d3);
		System.out.println(x);
		x = d1.compareTo(d1);
		System.out.println(x);
		
	}

}
