
public class Rechteck extends GeoObject{
	
	private double hoehe, breite;
	
	public Rechteck (double x, double y, double hoehe, double breite) {
		super(x, y);
		setHoehe(hoehe);
		setBreite(breite);
	}
	
	public double getHoehe() {
		return hoehe;
	}
	
	public void setHoehe(double hoehe) {
		this.hoehe = hoehe;
	}
	
	public double getBreite() {
		return hoehe;
	}
	public void setBreite(double breite) {
		this.breite = breite;
	}
	
	public double determineArea() {
		return this.breite * this.hoehe;
	}

	@Override
	public String toString() {
		return "Rechteck [x=" + x + ", y=" + y + ", Hoehe=" + hoehe + ", Breite=" + breite + ", Flaeche=" + this.determineArea() + "]";
	}
}
