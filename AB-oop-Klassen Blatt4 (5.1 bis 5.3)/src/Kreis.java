
public class Kreis extends GeoObject{
	
	private double radius;
	
	public Kreis (double x, double y, double radius) {
		super(x, y);
		setRadius(radius);
	}
	
	public void setRadius(double radius) {
		this.radius = radius;
	}
	
	public double getRadius() {
		return radius;
	}
	
	public double determineArea() {
		return 3.1415 * this.radius * this.radius;
	}

	@Override
	public String toString() {
		return "Rechteck [x=" + x + ", y=" + y + ", Radius=" + radius + ", Flaeche=" + this.determineArea() + "]";
	}
}
