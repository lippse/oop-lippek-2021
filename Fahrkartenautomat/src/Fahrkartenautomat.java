﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       double gesamtpreis = fahrkartenbestellungErfassen();
       
       // Geldeinwurf
       // -----------
       double rueckgabebetrag = fahrkartenBezahlen(gesamtpreis);

       // Fahrscheinausgabe
       // -----------------
       fahrkartenAusgeben();

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       rueckgeldAusgeben(rueckgabebetrag);
    }
    
    public static double fahrkartenbestellungErfassen() {
    	Scanner tastatur = new Scanner(System.in);
    	
        System.out.print("Zu zahlender Betrag (EURO): ");
        double zuZahlenderBetrag = tastatur.nextDouble();
        
        System.out.print("Anzahl der Tickets: ");
        int anzahlTickets = tastatur.nextInt();
        
        return zuZahlenderBetrag * anzahlTickets;
	}
    
    public static double fahrkartenBezahlen(double zuZahlen) {
    	Scanner tastatur = new Scanner(System.in);
    	double eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlen)
        {
     	   System.out.printf("Noch zu zahlen: %.2f Euro%n", (zuZahlen - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   double eingeworfeneMünze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
    	return eingezahlterGesamtbetrag - zuZahlen;
    }
    
    public static void fahrkartenAusgeben() {
    	System.out.println("\nFahrschein wird ausgegeben");
    	wait(2000);
        System.out.println("\n\n");
    }
    
    public static void rueckgeldAusgeben(double rückgabebetrag) {
    	int rückgabebetragInt = (int)(rückgabebetrag * 100);
        if(rückgabebetragInt > 0)
        {
     	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO %n", rückgabebetrag);
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetragInt >= 200) // 2 EURO-Münzen
            {
              muenzeAusgeben(2, "EURO");
 	          rückgabebetragInt -= 200;
            }
            while(rückgabebetragInt >= 100) // 1 EURO-Münzen
            {
              muenzeAusgeben(1, "EURO");
 	          rückgabebetragInt -= 100;
            }
            while(rückgabebetragInt >= 50) // 50 CENT-Münzen
            {
              muenzeAusgeben(50, "CENT");
 	          rückgabebetragInt -= 50;
            }
            while(rückgabebetragInt >= 20) // 20 CENT-Münzen
            {
              muenzeAusgeben(20, "CENT");
  	          rückgabebetragInt -= 20;
            }
            while(rückgabebetragInt >= 10) // 10 CENT-Münzen
            {
              muenzeAusgeben(10, "CENT");
 	          rückgabebetragInt -= 10;
            }
            while(rückgabebetragInt >= 5) // 5 CENT-Münzen 
            {
              muenzeAusgeben(5, "CENT");
  	          rückgabebetragInt -= 5;
            }
        }
  
        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt.");
    }
    
    public static void wait(int millisekunden) {
    for (int i = 0; i < 8; i++)
    {
       System.out.print("=");
       try {
			Thread.sleep(millisekunden / 8);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    }
    
    public static void muenzeAusgeben(int betrag, String einheit) {
   	  System.out.printf("%-2d %s %n", betrag, einheit);
    }
}

