
public class Unterklasse extends Oberklasse{
	
	public double objAttr = 100.0;
	public static int klAttr = 200;
	
	public void objMethode() {
		System.out.println("Unterklasse: objMethode");
	}
	
	public static void klMethode() {
		System.out.println("Unterklasse: klMethode");
	}

}
