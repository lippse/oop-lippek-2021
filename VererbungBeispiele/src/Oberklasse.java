
public class Oberklasse {
	
	public int objAttr = 1;
	public static int klAttr = 2;

	public void objMethode() {
		System.out.println("Oberklasse: objMethode");
	}

	public static void klMethode() {
		System.out.println("Oberklasse: klMethode");
	}
}
