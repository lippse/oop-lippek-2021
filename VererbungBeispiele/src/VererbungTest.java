
public class VererbungTest {

	public static void main(String[] args) {
		
		Oberklasse ok = new Oberklasse();
		Unterklasse uk = new Unterklasse();

		ok.objMethode();
		uk.objMethode();
		((Oberklasse)uk).objMethode();
		
		uk.klMethode();
		((Oberklasse)uk).klMethode();
		
		System.out.println(uk.objAttr);
	}

}
