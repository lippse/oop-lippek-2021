import java.util.Scanner;

public class ListePersonenDaten {

	public static void main(String[] args) {

		Scanner myScanner = new Scanner(System.in);
		System.out.println("Geben Sie Ihren Vornamen ein:");
		String vorname = myScanner.next();
		System.out.println("Geben Sie Ihren Nachnamen ein:");
		String nachname = myScanner.next();
		System.out.println("Geben Sie Ihr Alter ein:");
		int alter = myScanner.nextInt();
		System.out.printf("%-12s%s%n","Vorname:", vorname);
		System.out.printf("%-12s%s%n","Nachname:", nachname);
		System.out.printf("%-12s%d%n","Alter:", alter);
	}

}
