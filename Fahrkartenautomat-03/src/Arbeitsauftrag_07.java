import java.util.Scanner;

class Arbeitsauftrag_08
{
    public static void main(String[] args)
    {

    	while(true) {
	       double gesamtpreis = fahrkartenbestellungErfassen();
	       
	       // Geldeinwurf
	       // -----------
	       double rueckgabebetrag = fahrkartenBezahlen(gesamtpreis);
	
	       // Fahrscheinausgabe
	       // -----------------
	       fahrkartenAusgeben();
	
	       // R�ckgeldberechnung und -Ausgabe
	       // -------------------------------
	       rueckgeldAusgeben(rueckgabebetrag);
    
    	}
    }
    
    public static double fahrkartenbestellungErfassen() {
    	Scanner tastatur = new Scanner(System.in);
    	
        Ticket[] ticketarten = {
        		new Ticket("Einzelfahrschein Berlin AB", 2.9, 0),
        		new Ticket("Einzelfahrschein Berlin BC", 3.3, 0),
        		new Ticket("Einzelfahrschein Berlin ABC", 3.6, 0),
        		new Ticket("Kurzstrecke", 1.9, 0),
        		new Ticket("Tageskarte Berlin AB", 8.6, 0),
        		new Ticket("Tageskarte Berlin BC", 9.0, 0),
        		new Ticket("Tageskarte Berlin ABC", 9.6, 0),
        		new Ticket("Kleingruppen-Tageskarte Berlin AB", 23.5, 0),
        		new Ticket("Kleingruppen-Tageskarte Berlin BC", 24.3, 0),
        		new Ticket("Kleingruppen-Tageskarte Berlin ABC", 24.9, 0)
        		};
        System.out.println("Fahrkartenbestellvorgang:\n===========================\n");
        System.out.println("W�hlen Sie Ihre Wunschfahrkarte aus:");
        System.out.println("----------------------------------------------------------------------------");
        System.out.printf("|%-15s||%-40s||%-15s|%n","Auswahlnummer", "Bezeichnung", "Preis in Euro");
        System.out.println("----------------------------------------------------------------------------");
        for (int i=0; i<ticketarten.length; i++) {
        	System.out.printf("|%-15d||%-40s||%-15.2f|%n",i ,ticketarten[i].getBezeichner(), ticketarten[i].getPreis());  	
        }
        System.out.println("----------------------------------------------------------------------------");
        int wunschTicket;
        do {
        System.out.print("Ihre Wahl: ");      
        	wunschTicket = tastatur.nextInt();
        	if (wunschTicket < 1 || wunschTicket > ticketarten.length)
	    		System.out.println(">>falsche Eingabe<<");
        }while (wunschTicket < 1 || wunschTicket > ticketarten.length);
        System.out.print("Anzahl der Tickets: ");
        int anzahlTickets = tastatur.nextInt();
        
        while (anzahlTickets < 1 || anzahlTickets > 10) {
        	anzahlTickets = 1;
        	System.out.println("fehlerhafte Eingabe! Bitte geben Sie eine Anzahl zwischen 1 und 10 ein.");
        	anzahlTickets = tastatur.nextInt();
        }
        
        return ticketarten[wunschTicket].getPreis() * anzahlTickets;
	}
    
    public static double fahrkartenBezahlen(double zuZahlen) {
    	Scanner tastatur = new Scanner(System.in);
    	double eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlen)
        {
     	   System.out.printf("Noch zu zahlen: %.2f Euro%n", (zuZahlen - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, h�chstens 50 Euro): ");
     	   double eingeworfeneM�nze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneM�nze;
        }
    	return eingezahlterGesamtbetrag - zuZahlen;
    }
    
    public static void fahrkartenAusgeben() {
    	System.out.println("\nFahrschein wird ausgegeben");
    	wait(2000);
        System.out.println("\n\n");
    }
    
    public static void rueckgeldAusgeben(double r�ckgabebetrag) {
    	int r�ckgabebetragInt = (int)(r�ckgabebetrag * 100);
        if(r�ckgabebetragInt > 0)
        {
     	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f EURO %n", r�ckgabebetrag);
     	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

            while(r�ckgabebetragInt >= 200) // 2 EURO-M�nzen
            {
              muenzeAusgeben(2, "EURO");
 	          r�ckgabebetragInt -= 200;
            }
            while(r�ckgabebetragInt >= 100) // 1 EURO-M�nzen
            {
              muenzeAusgeben(1, "EURO");
 	          r�ckgabebetragInt -= 100;
            }
            while(r�ckgabebetragInt >= 50) // 50 CENT-M�nzen
            {
              muenzeAusgeben(50, "CENT");
 	          r�ckgabebetragInt -= 50;
            }
            while(r�ckgabebetragInt >= 20) // 20 CENT-M�nzen
            {
              muenzeAusgeben(20, "CENT");
  	          r�ckgabebetragInt -= 20;
            }
            while(r�ckgabebetragInt >= 10) // 10 CENT-M�nzen
            {
              muenzeAusgeben(10, "CENT");
 	          r�ckgabebetragInt -= 10;
            }
            while(r�ckgabebetragInt >= 5) // 5 CENT-M�nzen 
            {
              muenzeAusgeben(5, "CENT");
  	          r�ckgabebetragInt -= 5;
            }
        }
  
        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir w�nschen Ihnen eine gute Fahrt.\n=============================================\n\n\n\n");
    }
    
    public static void wait(int millisekunden) {
    for (int i = 0; i < 8; i++)
    {
       System.out.print("=");
       try {
			Thread.sleep(millisekunden / 8);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
    }
    }
    
    public static void muenzeAusgeben(int betrag, String einheit) {
   	  System.out.printf("%-2d %s %n", betrag, einheit);
    }
}

