
public class Ticket {
	private String bezeichner;
	private double preis;
	private int anzahl;
	
	public Ticket() {
		bezeichner = "Unbekannt";
		preis = 0.0;
		anzahl = 0;
	}
	
	public Ticket(String bezeichner, double preis, int anzahl) {
		setBezeichner(bezeichner);
		setPreis(preis);
		setAnzahl(anzahl);
	}

	public String getBezeichner() {
		return bezeichner;
	}

	public void setBezeichner(String bezeichner) {
		this.bezeichner = bezeichner;
	}

	public double getPreis() {
		return preis;
	}

	public void setPreis(double preis) {
		this.preis = preis;
	}

	public int getAnzahl() {
		return anzahl;
	}

	public void setAnzahl(int anzahl) {
		this.anzahl = anzahl;
	}

	@Override
	public String toString() {
		return "Ticket [bezeichner=" + bezeichner + ", preis=" + preis + ", anzahl=" + anzahl + "]";
	}
	
}
