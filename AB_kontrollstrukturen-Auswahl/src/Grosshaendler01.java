import java.util.Scanner;

public class Grosshaendler01 {

	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Geben Sie die Anzahl der Maeuse ein:");
		int anz = myScanner.nextInt();
		System.out.println("Geben Sie nun den Einzelpreis der Maeuse ein:");
		float preis = myScanner.nextFloat();
		
		float gesamtpreis = anz * preis;
		gesamtpreis += gesamtpreis * 0.19;
		
		if (anz<10) {
			gesamtpreis += 10; 
		}
		System.out.printf("der Gesamtpreis incl. MwSt. ist %.2f �", gesamtpreis);
	}

}
