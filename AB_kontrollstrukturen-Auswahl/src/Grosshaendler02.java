import java.util.Scanner;

public class Grosshaendler02 {

	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Geben Sie den Gesamtpreis der Maeuse ein:");
		float gesamtpreis = myScanner.nextFloat();

		if (gesamtpreis > 100) {
			gesamtpreis -= gesamtpreis*0.1;
		}
		else if (gesamtpreis > 500) {
			gesamtpreis -= gesamtpreis*0.15;
		}
		else {
			gesamtpreis -= gesamtpreis*0.2;
		}
		System.out.printf("der Gesamtpreis incl. MwSt. ist %.2f �", gesamtpreis);
	}

}
