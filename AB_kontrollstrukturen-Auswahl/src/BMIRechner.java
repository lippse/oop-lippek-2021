import java.util.Scanner;

public class BMIRechner {

	public static void main(String[] args) {

		Scanner myScanner = new Scanner(System.in);

		System.out.println("Geben Sie Ihr Koerpergewicht [in kg] ein:");
		float gewicht = myScanner.nextFloat();
		System.out.println("Geben Sie Ihre Groesse [in m] ein:");
		float groesse = myScanner.nextFloat();
		System.out.println("Bitte teilen Sie nun Ihr Geschlecht [m/w/d] mit:");
		char geschlecht = myScanner.next().charAt(0);

		float bmi = gewicht / (groesse * groesse);

		String klassifikation;

		if (geschlecht == 'm') {
			if (bmi < 20) {
				klassifikation = "Untergewicht";
			} else if (bmi < 25) {
				klassifikation = "Normalgewicht";
			} else {
				klassifikation = "Uebergewicht";
			}
		}

		else if (geschlecht == 'w') {
			if (bmi < 19) {
				klassifikation = "Untergewicht";
			} else if (bmi < 24) {
				klassifikation = "Normalgewicht";
			} else {
				klassifikation = "Uebergewicht";
			}
		} else {
			klassifikation = "unbekannt";
		}

		System.out.printf("Ihre Klassifikation ist %s (BMI = %.2f)", klassifikation, bmi);
	}

}
