
public class Aufgabe_4 {
	public static void main(String[] args) {
		int lottozahlen [] = {3, 7, 12, 18, 37, 42};
		System.out.print("[ ");
		for (int i = 0; i < lottozahlen.length; i++)
			System.out.print(lottozahlen[i] + " ");
		System.out.print("]\n");
		istDieZahlEnthalten(12, lottozahlen);
		istDieZahlEnthalten(13, lottozahlen);
		
	}
	
	public static void istDieZahlEnthalten(int zahl, int [] array) {
		boolean istEnthalten = false;
		for (int i : array) {
			if (zahl == i)
				istEnthalten = true;
			break;
		}
		if (istEnthalten)
			System.out.printf("Die Zahl %d ist in der Ziehung enthalten.\n", zahl);
		else 
			System.out.printf("Die Zahl %d ist nicht in der Ziehung enthalten.\n", zahl);
	}
}
