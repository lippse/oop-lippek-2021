
public class Division {
	
	public static void main(String[] args) throws ArithmeticException{
		Division d = new Division();
		try {
			System.out.println(d.division(1, 1));
			System.out.println(d.division(1, 0));
		} catch (ArithmeticException ex) {
			System.out.println("Division durch Null");
		}
	}
	
	public double division(int zaehler, int nenner) throws ArithmeticException{
		if (nenner == 0)
			throw new ArithmeticException();
		return zaehler / nenner;
	}
}
