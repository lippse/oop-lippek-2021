
public class DatumTest {

	public static void main(String[] args) {
		
		try {
			Datum d1 = new Datum(1, 1, 1970);
			Datum d2 = new Datum(1, 1, 1870);
		} catch (FalscherTagException ex) {
			System.out.println(ex);
		} catch (FalscherMonatException ex) {
			System.out.println(ex);
		} catch (FalschesJahrException ex) {
			System.out.println(ex);
		}
		
		try {
			Datum d1 = new Datum(1, 1, 1970);
			Datum d2 = new Datum(1, 13, 1970);
		} catch (FalscherTagException ex) {
			System.out.println(ex);
		} catch (FalscherMonatException ex) {
			System.out.println(ex);
		} catch (FalschesJahrException ex) {
			System.out.println(ex);
		}
		
		try {
			Datum d1 = new Datum(1, 1, 1970);
			Datum d2 = new Datum(35, 1, 1970);
		} catch (FalscherTagException ex) {
			System.out.println(ex);
		} catch (FalscherMonatException ex) {
			System.out.println(ex);
		} catch (FalschesJahrException ex) {
			System.out.println(ex);
		}
	}

}
