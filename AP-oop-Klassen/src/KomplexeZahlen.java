
public class KomplexeZahlen {
	
	private double re, im;
	
	public KomplexeZahlen() {
		re = 0.0;
		im = 0.0;
	}
	
	public KomplexeZahlen(double re, double im) {
		this.re = re;
		this.im = im;
	}
	
	public double getReal () {
		return re;
	}
	
	public double getImag () {
		return im;
	}
	
	public void setReal (double re) {
		this.re = re;
	}
	
	public void setImag (double im) {
		this.re = im;
	}
	
	@Override
	public String toString () {
		return "[ Realteil: " + re + " Imaginaerteil: " + im + " ]";
	}
	
	@Override
	public boolean equals(Object obj) {
		if ( obj instanceof KomplexeZahlen) {
			KomplexeZahlen k = (KomplexeZahlen) obj;
		if (this.im == k.getImag() && this.re == k.getReal())
			return true;
		else
			return false;
		}
		return false;
	}
	
	public static KomplexeZahlen multiplyComplex (KomplexeZahlen k1, KomplexeZahlen k2) {
		KomplexeZahlen z = new KomplexeZahlen();
		z.re = (k1.re * k2.re) - (k1.im * k2.im); 
		z.im = (k1.re * k2.im) + (k2.re * k1.im);
		return z;
	}
	
	public KomplexeZahlen multiplyComplex (KomplexeZahlen k) {
		KomplexeZahlen z = new KomplexeZahlen();
		z.re = (k.re * this.re) - (k.im * this.im); 
		z.im = (k.re * this.im) + (this.re * k.im);
		return z;
	}
}
