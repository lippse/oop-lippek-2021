
public class DatumTest {
	
	public static void main(String[] args) {

		Datum d1, d2;

		d1 = new Datum();
		
		System.out.println(d1);

		d1.setTag(1);
		d1.setMonat(10);
		d1.setJahr(1993);
		
		System.out.println(d1.toString());
		
		d2 = new Datum(3, 5, 2021);
		System.out.println(d1.berechneQuartal(d2));
		
		System.out.println(d2);
		if (d1.equals(d2))
			System.out.println("gleich");
		else
			System.out.println("ungleich");
		
		d2 = new Datum(1, 10, 1993);
		
		System.out.println(d2);
		if (d1.equals(d2))
			System.out.println("gleich");
		else
			System.out.println("ungleich");
		
		System.out.println(d1.berechneQuartal(d1));
	}
}
