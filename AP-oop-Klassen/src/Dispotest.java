
public class Dispotest {

	public static void main(String[] args) {
		
		Bankkonto b1 = new Bankkonto("Chris");
		Dispokonto d1 = new Dispokonto("Max", 100);
		
		d1.einzahlen(100);
		b1.einzahlen(100);

		System.out.println(d1.auszahlen(300));
		System.out.println(b1.auszahlen(300));
		
		Bankkonto[] Konten = new Bankkonto[4];
		Konten[0] = b1;
		Konten[1] = d1;
		Konten[2] = new Bankkonto("Franzi");
		Konten[3] = new Dispokonto("Frida", 500);
		
		for (int i = 0; i < Konten.length; i++)
			System.out.println(Konten[i]);
	}

}
