
public class Person {

	private String name;
	private Datum geburtsdatum;
	private int nummer;
	private static int anzahl;
	
	public Person() {
		this.name = "Unbekannt";
		this.geburtsdatum = new Datum();
		this.nummer = ++anzahl;
	}
	
	public Person(String name, Datum geburtsdatum) {
		setName(name);
		setGeburtsdatum(geburtsdatum);
		this.nummer = ++anzahl;
	}

	public String getName() {
		return name;
	}
	
	public Datum getGeburtsdatum() {
		return geburtsdatum;
	}
	
	public static int getAnzahl() {
		return anzahl;
	}

	public int getNummer() {
		return nummer;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public void setGeburtsdatum(Datum geburtsdatum) {
			this.geburtsdatum = geburtsdatum;
	}
	
	@Override
	public String toString() {
		return "[ Name: " + this.name + ", Geburtsdatum: " + this.geburtsdatum
				+ ", Nummer: " + nummer + ", Anzahl: " + anzahl + " ]"; 
	}
	
	@Override
	public boolean equals(Object obj) {
//		if (obj == null)
//			return false;
		
		if ( obj instanceof Person) {
		Person p = (Person) obj;
		if (this.name.equals(p.getName()) && 
			this.geburtsdatum.equals(p.getGeburtsdatum()))
			return true;
		else
			return false;
		}
		return false;
	}
}
