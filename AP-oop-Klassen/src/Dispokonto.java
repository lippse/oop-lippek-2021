
public class Dispokonto extends Bankkonto{
	private double dispokredit;
	
	public Dispokonto(String inhaber, double dispokredit) {
		super(inhaber);
		this.dispokredit = dispokredit;
	}

	public double getDispokredit() {
		return dispokredit;
	}

	public void setDispokredit(double dispokredit) {
		this.dispokredit = dispokredit;
	}
	
	@Override
	public double auszahlen(double betrag) {
		if (betrag <= (kontostand + dispokredit)) {
			kontostand -= betrag;
			return betrag;
		}
		else {
			double temp = kontostand + dispokredit;
			kontostand -= temp;
			return temp;
		}
	}

	@Override
	public String toString() {
		return super.toString() + ", Dispo: " + dispokredit;
	}
}