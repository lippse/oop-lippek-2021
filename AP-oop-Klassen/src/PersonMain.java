
public class PersonMain {

	public static void main(String[] args) {
		
		Datum d = new Datum();
		Person p = new Person();
		
		System.out.println(p);

		Datum d1, d2;
		Person p1, p2;
		
		d1 = new Datum(1, 10, 1993);
		p1 = new Person("Chris", d1);
		
		d2 = new Datum(1, 10, 1993);
		p2 = new Person("Chris", d2);
		
		if (p1.equals(p2))
			System.out.println("gleich");
		else
			System.out.println("ungleich");
		
		System.out.println(p2);
		System.out.println(p1);
	}

}
