
public class Bankkonto {
	private String inhaber;
	protected int kontonummer;
	protected double kontostand;
	protected static int anzahl = 0;
	
	public Bankkonto (String inhaber) {
		setInhaber(inhaber);
		kontonummer = ++anzahl;
		kontostand = 0.0;
	}
	
	public String getInhaber() {
		return inhaber;
	}

	public void setInhaber(String inhaber) {
		this.inhaber = inhaber;
	}

	public int getKontonummer() {
		return kontonummer;
	}

	public double getKontostand() {
		return kontostand;
	}
	
	public void einzahlen(double betrag) {
		if (betrag > 0)
			kontostand += betrag;
		else 
			System.out.println("zum Auszahlen bitte die Methode auszahlen verwenden!");
	}
	
	public double auszahlen(double betrag) {
			kontostand -= betrag;
			return betrag;
	}
	
	@Override
	public String toString() {
		return "Kontoinhaber: " + getInhaber() + ", Kontostand: " + getKontostand() + ", Kontonummer: " + getKontonummer();
	}
}
