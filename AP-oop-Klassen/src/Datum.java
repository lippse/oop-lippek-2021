
public class Datum {

	private int tag, monat, jahr;
	
	public Datum() {
		setTag(1);
		setMonat(1);
		setJahr(1970);
	}
	
	public Datum(int tag, int monat, int jahr) {
		setTag(tag);
		setMonat(monat);
		setJahr(jahr);
	}
	
	public int getTag() {
		return tag;
	}
	
	public int getMonat() {
		return monat;
	}
	
	public int getJahr() {
		return jahr;
	}
	
	public void setTag(int tag) {
		if (tag > 0 && tag < 32)
			this.tag = tag;
		else 
			System.out.println("Error");
	}
	
	public void setMonat(int monat) {
		if (monat > 0 && monat < 13)
			this.monat = monat;
		else 
			System.out.println("Error");
	}
	
	public void setJahr(int jahr) {
		if (jahr > 1900 && jahr < 2022)
			this.jahr = jahr;
		else 
			System.out.println("Error");
	}
	
	@Override
	public String toString() {
		return tag + "." + monat + "." + jahr; 
	}
	
	@Override
	public boolean equals(Object obj) {
		
		if ( obj instanceof Datum) {
		Datum d = (Datum) obj;
		if (this.jahr == d.getJahr() && 
			this.monat == d.getMonat() &&
			this.tag == d.getTag())
			return true;
		else
			return false;
		}
		return false;
	}
	
	public static byte berechneQuartal(Datum d) {
		if (d.getMonat() < 4)
			return 1;
		else if (d.getMonat() < 7)
			return 2;
		else if (d.getMonat() < 10)
			return 3;
		else
			return 4;
	}
}

