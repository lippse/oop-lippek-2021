
public class KomplexeZahlenTest {
	public static void main(String[] args) {
		
		KomplexeZahlen k1, k2, k3, k4;
		
		k1 = new KomplexeZahlen(0.5, 0.2);
		k2 = new KomplexeZahlen(0.3, 0.3);
		k3 = new KomplexeZahlen();
		k4 = new KomplexeZahlen();
		
		System.out.println(k1);
		
		if ( k1.equals(k2))
			System.out.println("gleich");
		else
			System.out.println("ungleich");
		
		k3 = KomplexeZahlen.multiplyComplex(k1, k2);
		k4 = k1.multiplyComplex(k2);
		
		System.out.println(k3);
		
		if ( k3.equals(k4))
			System.out.println("gleich");
		else
			System.out.println("ungleich");
	}
}
