import java.util.Scanner;

public class Aufgabe_2c {
	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Geben Sie einen begrenzenden Wert ein:");
		int n = myScanner.nextInt();
		int summe = 0;
		for (int i = 1; i <= n; i += 2) {
			summe += i;
		}
		System.out.println("Die Summe f�r C betr�gt: " + summe);
	}
	
}
