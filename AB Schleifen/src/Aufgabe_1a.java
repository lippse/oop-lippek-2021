import java.util.Scanner;

public class Aufgabe_1a {
	
	public static void main(String[] args) {
		
	Scanner myScanner = new Scanner(System.in);
	System.out.println("Geben Sie eine Zahl ein:");
	int n = myScanner.nextInt();
	int x = 1;
	while (x <= n) {
		System.out.print(x);
		if(x<n) {
			System.out.print(",");
		}
		x++;
	}
	
	}
}
