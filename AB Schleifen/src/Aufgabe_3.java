import java.util.Scanner;

public class Aufgabe_3 {
	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Geben Sie eine Zahl ein:");
		int n = myScanner.nextInt();
        int quersumme, rest;
        quersumme = 0;
        while (n > 0) {
            rest = n % 10;
            quersumme = quersumme + rest;
            n = n / 10;
        }
        System.out.println("Quersumme ist " + quersumme ) ;
	}
}
