
public class PersonTest {

	public static void main(String[] args) {

		Person p1, p2, p3, p4;

		p1 = new Person();
		
		System.out.println(Person.getAnzahl());
		
		p2 = new Person("Chris", 27);
		p3 = new Person();
		p3.setName("Chris");
		p4 = null;
		
		System.out.println(Person.getAnzahl());
		System.out.println(p1.getAnzahl());
		
		String str = new String("bla");

		System.out.println(p1.getName());
		System.out.println(p1.getAlter());

		p1.setName("Max");
		p1.setAlter(27);
		
		p3.setName("Max");
		p3.setAlter(27);

		if ( p1 == p3 )  //falsch, geht nicht mit Objekten
			System.out.println("gleich");
		else
			System.out.println("ungleich");
		
		if ( p1.equals(p3))
			System.out.println("gleich");
		else
			System.out.println("ungleich");
		
		if ( p1.equals(p4)) //"null" Abgedeckt mit instanceof
			System.out.println("gleich");
		else
			System.out.println("ungleich");
		
		if ( p1.equals(str)) //anderer Datentyp abgedeckt mit instanceof
			System.out.println("gleich");
		else
			System.out.println("ungleich");
		
//		System.out.println(p1.getName());
//		System.out.println(p1.getAlter());
		System.out.println(p1.toString());
		
		System.out.println(p2.toString());
		
		System.out.println(p2);
	}
}
