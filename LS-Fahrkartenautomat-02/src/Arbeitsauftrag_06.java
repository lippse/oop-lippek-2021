import java.util.Scanner;

class Arbeitsauftrag_06
{
    public static void main(String[] args)
    {

    	while(true) {
	       double gesamtpreis = fahrkartenbestellungErfassen();
	       
	       // Geldeinwurf
	       // -----------
	       double rueckgabebetrag = fahrkartenBezahlen(gesamtpreis);
	
	       // Fahrscheinausgabe
	       // -----------------
	       fahrkartenAusgeben();
	
	       // R�ckgeldberechnung und -Ausgabe
	       // -------------------------------
	       rueckgeldAusgeben(rueckgabebetrag);
    
    	}
    }
    public static double fahrkartenbestellungErfassen() {
    	Scanner tastatur = new Scanner(System.in);
    	
        System.out.println("Fahrkartenbestellvorgang:\n===========================\n");
        System.out.println("W�hlen Sie Ihre Wunschfahrkarte f�r Berlin AB aus:");
        System.out.println("  Einzelfahrschein Regeltarif AB [2,90 EUR] (1)");
        System.out.println("  Tageskarte Regeltarif AB [8,60 EUR] (2)");
        System.out.println("  Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");
        
        double zuZahlenderBetrag = 0;
        boolean nochmal = true;
        while (nochmal) {
        	System.out.print("Ihre Wahl: ");      
        	int wunschTicket = tastatur.nextInt();
        	nochmal = false;
	        switch (wunschTicket) {
	        case 1:
	        	zuZahlenderBetrag = 2.9;
	        	break;
	        case 2:
	        	zuZahlenderBetrag = 8.6;
	        	break;
	        case 3:
	        	zuZahlenderBetrag = 23.5;
	        	break;
	        default:
	        	System.out.println(">>falsche Eingabe<<");
	        	nochmal = true;
	            break;
	        }
        }
        System.out.print("Anzahl der Tickets: ");
        int anzahlTickets = tastatur.nextInt();
        
        while (anzahlTickets < 1 || anzahlTickets > 10) {
        	anzahlTickets = 1;
        	 System.out.println("fehlerhafte Eingabe! Bitte geben Sie eine Anzahl zwischen 1 und 10 ein.");
        	 anzahlTickets = tastatur.nextInt();
        }
        
        return zuZahlenderBetrag * anzahlTickets;
	}
    
    public static double fahrkartenBezahlen(double zuZahlen) {
    	Scanner tastatur = new Scanner(System.in);
    	double eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlen)
        {
     	   System.out.printf("Noch zu zahlen: %.2f Euro%n", (zuZahlen - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, h�chstens 50 Euro): ");
     	   double eingeworfeneM�nze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneM�nze;
        }
    	return eingezahlterGesamtbetrag - zuZahlen;
    }
    
    public static void fahrkartenAusgeben() {
    	System.out.println("\nFahrschein wird ausgegeben");
    	wait(2000);
        System.out.println("\n\n");
    }
    
    public static void rueckgeldAusgeben(double r�ckgabebetrag) {
    	int r�ckgabebetragInt = (int)(r�ckgabebetrag * 100);
        if(r�ckgabebetragInt > 0)
        {
     	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f EURO %n", r�ckgabebetrag);
     	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

            while(r�ckgabebetragInt >= 200) // 2 EURO-M�nzen
            {
              muenzeAusgeben(2, "EURO");
 	          r�ckgabebetragInt -= 200;
            }
            while(r�ckgabebetragInt >= 100) // 1 EURO-M�nzen
            {
              muenzeAusgeben(1, "EURO");
 	          r�ckgabebetragInt -= 100;
            }
            while(r�ckgabebetragInt >= 50) // 50 CENT-M�nzen
            {
              muenzeAusgeben(50, "CENT");
 	          r�ckgabebetragInt -= 50;
            }
            while(r�ckgabebetragInt >= 20) // 20 CENT-M�nzen
            {
              muenzeAusgeben(20, "CENT");
  	          r�ckgabebetragInt -= 20;
            }
            while(r�ckgabebetragInt >= 10) // 10 CENT-M�nzen
            {
              muenzeAusgeben(10, "CENT");
 	          r�ckgabebetragInt -= 10;
            }
            while(r�ckgabebetragInt >= 5) // 5 CENT-M�nzen 
            {
              muenzeAusgeben(5, "CENT");
  	          r�ckgabebetragInt -= 5;
            }
        }
  
        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir w�nschen Ihnen eine gute Fahrt.\n\n");
    }
    
    public static void wait(int millisekunden) {
    for (int i = 0; i < 8; i++)
    {
       System.out.print("=");
       try {
			Thread.sleep(millisekunden / 8);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    }
    
    public static void muenzeAusgeben(int betrag, String einheit) {
   	  System.out.printf("%-2d %s %n", betrag, einheit);
    }
}

